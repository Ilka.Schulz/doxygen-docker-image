FROM debian:stable-slim
MAINTAINER doygendockerimages@schulz.com.de

ARG PACKAGES="doxygen graphviz qhelpgenerator-qt5"
ARG DEBIAN_FRONTEND=noninteractive

RUN apt update && \
    apt install -y $PACKAGES && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /data
VOLUME [ "/data" ]
