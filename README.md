# Doxygen Docker Image

![build badge](https://img.shields.io/gitlab/pipeline-status/Ilka.Schulz/doxygen-docker-image?branch=main)

This is a simple Docker image with Doxygen und related tools. The Dockerfile speaks for itself...